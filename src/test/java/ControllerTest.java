import com.despegar.sparkjava.test.SparkServer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import service.AccountService;
import spark.servlet.SparkApplication;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.*;

public class ControllerTest {
    List<Transfer> transferList = new ArrayList<>();

    public static class ApplicationMainTest implements SparkApplication {
        public void init() {
            ApplicationMain.main(null);
        }
    }

    @ClassRule
    public static final SparkServer<ApplicationMainTest> testServer = new SparkServer<>(ApplicationMainTest.class);

    @Before
    public void init() {
        get("http://localhost:3333/account/create?account=203");
        get("http://localhost:3333/account/create?account=204");
        get("http://localhost:3333/account/addmoney?account=204&balance=1500000000");
        get("http://localhost:3333/account/addmoney?account=203&balance=1500000000");

        for (int i = 0; i < 1000; i++) {
            if (i % 2 == 0) {
                transferList.add(new Transfer().create("204", "203", i));
            } else {
                transferList.add(new Transfer().create("203", "204", i));
            }
        }
    }

    @Test
    public void testTransfer() {
            transferList.parallelStream()
                .forEach(t -> get("http://localhost:3333/account/transfer?from=" + t.from + "&to=" +
                    t.to + "&amount=" + t.amount));

        String first = get("http://localhost:3333/account/204").then().extract().asString();
        String second = get("http://localhost:3333/account/203").then().extract().asString();
        System.out.println(second + ", " + first);
        Assert.assertTrue(first, first.equals("204 balance: 1500000500"));
        Assert.assertTrue(second, second.equals("203 balance: 1499999500"));
    }


    private class Transfer {
        String from;
        String to;
        long amount;

        public Transfer create(String from, String to, long amount) {
            Transfer result = new Transfer();
            result.amount = amount;
            result.from = from;
            result.to = to;
            return result;
        }
    }
}

