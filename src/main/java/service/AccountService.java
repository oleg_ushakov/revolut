package service;

import entity.Account;

import java.util.Optional;

/**
 * @author Oleg Ushakov. 2019
 */
public interface AccountService {
  Optional<Account> createAccount(String accountNumber);

  Optional<Account> findByNumber(String accountNumber);

  boolean transferMoney(String sourceAccount, String targetAccount, Long amount) throws InterruptedException;

  Optional<Account> setBalance(String accountNumber, Long amount);

  void withdrawMoney(Account account, Long amount);

  void addMoney(Account account, Long amount);

  Optional<Account> addMoney(String accountId, Long amount);

}
