package service.impl;

import entity.Account;
import repository.AccountRepository;
import service.AccountService;

import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

/**
 * @author Oleg Ushakov. 2019
 */
public final class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final static TimeUnit UNIT = NANOSECONDS;
    private final static int TIMEOUT = 1000;

    @Override
    public Optional<Account> createAccount(String accountNumber) {
        return accountRepository.createAccount(accountNumber);
    }

    @Override
    public Optional<Account> findByNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public final boolean transferMoney(String sourceId, String targetId, Long amount) throws InterruptedException {
        final long fixedDelay = getFixedDelayComponentNanos(TIMEOUT, UNIT);
        final long randMod = getRandomDelayModulusNanos(TIMEOUT, UNIT);
        final long stopTime = System.nanoTime() + UNIT.toNanos(TIMEOUT);
        try {
            final Account sourceAccount = findByNumber(sourceId).orElseThrow(() -> new IllegalArgumentException("sourceId: " + sourceId));
            final Account targetAccount = findByNumber(targetId).orElseThrow(() -> new IllegalArgumentException("targetId: " + targetId));
            Account[] accounts = new Account[2];
            final int compareResult = sourceAccount.getAccountNumber().compareTo(targetAccount.getAccountNumber());
            if (compareResult == 0) {
                return true;
            } else if (compareResult < 0) {
                accounts[0] = sourceAccount;
                accounts[1] = targetAccount;
            } else {
                accounts[0] = targetAccount;
                accounts[1] = sourceAccount;
            }
            synchronized (accounts[0]) {
                synchronized (accounts[1]) {
                    if (sourceAccount.getBalance().intValue() < amount) {
                        return false;
                    }
                    withdrawMoney(sourceAccount, amount);
                    addMoney(targetAccount, amount);
                    return true;
                }
            }
        } catch (
            IllegalArgumentException e) {
            System.err.println(e);
            return false;
        }
    }

    @Override
    public Optional<Account> setBalance(String accountNumber, Long amount) {
        Optional<Account> result = findByNumber(accountNumber);
        result.ifPresent(a -> {
            a.setBalance(amount);
            accountRepository.saveAccount(a);
        });
        return result;
    }

    @Override
    public final void withdrawMoney(Account account, Long amount) {
        accountRepository.addMoney(account, -amount);
    }

    @Override
    public final void addMoney(Account account, Long amount) {
        accountRepository.addMoney(account, amount);
    }

    @Override
    public final Optional<Account> addMoney(String accountId, Long amount) {
        Optional<Account> result = findByNumber(accountId);
        result.ifPresent(a -> {
            accountRepository.addMoney(a, amount);
        });
        return result;
    }

    private void save(Account account) {
        accountRepository.saveAccount(account);
    }

    static long getFixedDelayComponentNanos(long timeout, TimeUnit unit) {
        return unit.toNanos(timeout);
    }

    static long getRandomDelayModulusNanos(long timeout, TimeUnit unit) {
        return unit.toNanos(timeout);
    }

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    private static Random rnd = new Random();
}
