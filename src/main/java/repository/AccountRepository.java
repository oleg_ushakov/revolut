package repository;

import entity.Account;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Oleg Ushakov. 2019
 */
public class AccountRepository {
    private Map<String, Account> accountList = new ConcurrentHashMap<>();

    public Optional<Account> createAccount(String number) {
        if (number == null || accountList.containsKey(number)) {
            return Optional.empty();
        }
        Account account = new Account(number, 0L);
        accountList.put(number, account);
        return Optional.of(account);
    }

    public Optional<Account> saveAccount(Account accountEntity) {
        return findByAccountNumber(accountEntity.getAccountNumber())
            .map(a -> {
                accountList.put(a.getAccountNumber(), accountEntity);
                return Optional.of(accountEntity);
            })
            .orElse(Optional.empty());
    }


    public Optional<Account> findByAccountNumber(String number) {
        return Optional.of(accountList.get(number));
    }

    public final Account addMoney(Account account, Long amount) {
        return accountList.compute(account.getAccountNumber(), (key, value) -> {
            value.addToBalance(amount);
            return value;
        });
    }

}
