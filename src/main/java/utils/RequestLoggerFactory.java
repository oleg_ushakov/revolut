package utils;

import org.eclipse.jetty.server.AbstractNCSARequestLog;
import org.eclipse.jetty.server.RequestLogWriter;
import org.slf4j.Logger;

import java.io.IOException;

/**
 * @author Oleg Ushakov. 2019
 */
public class RequestLoggerFactory {
  private Logger logger;

  public RequestLoggerFactory(Logger logger) {
    this.logger = logger;
  }

  AbstractNCSARequestLog create() {
    return new AbstractNCSARequestLog(new RequestLogWriter()) {
      @Override
      protected boolean isEnabled() {
        return true;
      }

      @Override
      public void write(String s) throws IOException {
        logger.info(s);
      }
    };
  }
}
