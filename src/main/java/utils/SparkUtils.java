package utils;

import org.eclipse.jetty.server.AbstractNCSARequestLog;
import org.slf4j.Logger;
import spark.embeddedserver.EmbeddedServers;
import spark.embeddedserver.jetty.EmbeddedJettyFactory;

/**
 * @author Oleg Ushakov. 2019
 */
public class SparkUtils {
  public static void createServerWithRequestLog(Logger logger) {
        EmbeddedJettyFactory factory = createEmbeddedJettyFactoryWithRequestLog(logger);
        EmbeddedServers.add(EmbeddedServers.Identifiers.JETTY, factory);
    }

    private static EmbeddedJettyFactory createEmbeddedJettyFactoryWithRequestLog(org.slf4j.Logger logger) {
        AbstractNCSARequestLog requestLog = new RequestLoggerFactory(logger).create();
        return new EmbeddedJettyFactoryConstructor(requestLog).create();
    }
}
