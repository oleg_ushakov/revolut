package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Oleg Ushakov. 2019
 */
public class SystemResource {
  private static final Logger log = LoggerFactory.getLogger(SystemResource.class);
}

