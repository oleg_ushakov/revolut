import entity.Account;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import repository.AccountRepository;
import service.AccountService;
import service.impl.AccountServiceImpl;
import utils.SparkUtils;

import static spark.Spark.get;
import static spark.Spark.port;

/**
 * @author Oleg Ushakov. 2019
 */
public class ApplicationMain {
    private static final Logger log = LoggerFactory.getLogger(ApplicationMain.class);

    public static void main(String[] args) {
        AccountRepository repository = new AccountRepository();
        AccountService service = new AccountServiceImpl(repository);

        port(3333);
        SparkUtils.createServerWithRequestLog(log);

        get("/account/create", (req, res) -> {
            res.status(HttpStatus.OK_200);
            return service.createAccount(req.queryParams("account"))
                .map(Account::getAccountNumber)
                .orElse("Operation failed");
        });

        get("/account/addmoney", (req, res) -> {
            res.status(HttpStatus.OK_200);
            return service.addMoney(req.queryParams("account"), Long.valueOf(req.queryParams("balance")))
                .map(a -> a.getAccountNumber() + " balance: " + a.getBalance())
                .orElse("Operation failed");
        });

        get("/account/transfer", (req, res) -> {
            res.status(HttpStatus.OK_200);
            String from = req.queryParams("from");
            String to = req.queryParams("to");
            Long amount = Long.valueOf(req.queryParams("amount"));
            return service.transferMoney(from, to, amount)
                ? "Operation finished successfully"
                : "Operation failed";
        });
        get("/account/:number", (req, res) -> {
            res.status(HttpStatus.OK_200);
            return service.findByNumber(req.params(":number"))
                .map(a -> a.getAccountNumber() + " balance: " + a.getBalance())
                .orElse("Operation failed");
        });
    }
}
