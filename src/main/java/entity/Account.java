package entity;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Oleg Ushakov. 2019
 */
public final class Account {
    private String accountNumber;
    private final AtomicLong balance;

    public Account(String number, Long balance) {
        this.balance =  new AtomicLong(balance);
        this.accountNumber = number;
    }

    public final String getAccountNumber() {
        return accountNumber;
    }

    public final AtomicLong getBalance() {
        return balance;
    }

    public final void setBalance(Long balance) {
        this.balance.getAndSet(balance);
    }

    public final void addToBalance(long amount){
      balance.getAndAdd(amount);
    }
}
